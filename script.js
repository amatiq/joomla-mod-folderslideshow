jQuery(document).ready(function() {

  const params = Joomla.getOptions('mod_folderslideshow');
  console.log(params);

  var change_img_time 	= params.change_img_time;
  var transition_speed	= params.transition_speed;
  var simple_slideshow	= jQuery(".folderslideshow"),
      listItems 		= simple_slideshow.children('img'),
      listLen			= listItems.length,
      i 				= 0,

  changeList = function () {
    listItems.eq(i).fadeOut(transition_speed, function () {
      i += 1;
      if (i === listLen)
        i = 0;
      listItems.eq(i).fadeIn(transition_speed);
    });
  };

  listItems.not(':first').hide();
  setInterval(changeList, change_img_time);

});
