<?php
// No direct access
defined('_JEXEC') or die; ?>

<div class="folderslideshow">

  <?php
  foreach ($slideshow as $image) {
    echo JHtml::_(
      'image',
      $image->folder . '/' . htmlspecialchars($image->name, ENT_COMPAT, 'UTF-8'),
      htmlspecialchars($image->name, ENT_COMPAT, 'UTF-8')
    );
  }
  ?>

</div>
