<?php
/**
 * Folder Slideshow! Module Entry Point
 *
 * @package    Joomla.Tutorials
 * @subpackage Modules
 * @license    GNU/GPL, see LICENSE.php
 * @link       http://docs.joomla.org/J3.x:Creating_a_simple_module/Developing_a_Basic_Module
 * mod_helloworld is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

// No direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';

use Joomla\CMS\Factory;
$document = Factory::getDocument();
$options = array("version" => "auto");
$attributes = array("defer" => "defer");
$document->addScript(JURI::root() . 'modules/mod_folderslideshow/script.js', $options, $attributes);
$document->addScriptOptions('mod_folderslideshow', $params);
$document->addStyleSheet(JURI::root() . "modules/mod_folderslideshow/style.css", $options);

$folder = ModFolderSlideshowHelper::getFolder($params);
$images = ModFolderSlideshowHelper::getImages($params, $folder);

$slideshow = ModFolderSlideshowHelper::getSlideshow($params, $images);
require JModuleHelper::getLayoutPath('mod_folderslideshow');
