<?php
/**
 * Helper class for Folder Slidedshow module
 *
 * @package    Joomla.Tutorials
 * @subpackage Modules
 * @link http://docs.joomla.org/J3.x:Creating_a_simple_module/Developing_a_Basic_Module
 * @license        GNU/GPL, see LICENSE.php
 * mod_helloworld is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

use Joomla\String\StringHelper;

class ModFolderSlideshowHelper
{
    /**
     * Retrieves the hello message
     *
     * @param   array  $params An object containing the module parameters
     *
     * @access public
     */
    public static function getSlideshow($params, $images)
    {
        return $images;
    }

    	/**
	 * Retrieves images from a specific folder
	 *
	 * @param   \Joomla\Registry\Registry  &$params  module params
	 * @param   string                     $folder   folder to get the images from
	 *
	 * @return array
	 */
	public static function getImages(&$params, $folder)
	{
		$type   = $params->get('type', 'jpg');
		$files  = array();
		$images = array();

		$dir = JPATH_BASE . '/' . $folder;

		// Check if directory exists
		if (is_dir($dir))
		{
			if ($handle = opendir($dir))
			{
				while (false !== ($file = readdir($handle)))
				{
					if ($file !== '.' && $file !== '..' && $file !== 'CVS' && $file !== 'index.html')
					{
						$files[] = $file;
					}
				}
			}

			closedir($handle);

			$i = 0;

			foreach ($files as $img)
			{
				if (!is_dir($dir . '/' . $img) && preg_match('/' . $type . '/', $img))
				{
					$images[$i] = new stdClass;

					$images[$i]->name   = $img;
					$images[$i]->folder = $folder;
					$i++;
				}
			}
		}

		return $images;
	}


	/**
	 * Get sanitized folder
	 *
	 * @param   \Joomla\Registry\Registry  &$params  module params objects
	 *
	 * @return  mixed
	 */
	public static function getFolder(&$params) {

		$folder   = $params->get('folder');

		// If folder includes absolute path, remove
		if (StringHelper::strpos($folder, JPATH_SITE) === 0)
		{
			$folder = str_replace(JPATH_BASE, '', $folder);
		}

		return str_replace(array('\\', '/'), DIRECTORY_SEPARATOR, $folder);
	}
}
